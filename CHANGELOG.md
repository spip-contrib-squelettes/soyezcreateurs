# Changelog

All notable changes to this project will be documented in this file.

## [6.3.0] - 2025-02-27

### 🚀 Features

- Migration des galeries en albums (Rip le vénérable Portfolio)

### 🐛 Bug Fixes

- RIP ce joli formulaire de changement de statut ergonomique, perdu à partir de SPIP 4.3
- En cas de configuration de la branche en blog, les sites référencés étaient affichés 2 fois
- Les notes de bas de page présentes dans le chanmp Descriptif étaient en double
- Certains robots ne respectent pas le nofollow : éviter de polluer l'URL

## [6.2.6] - 2025-01-30

### 🐛 Bug Fixes

- fix : prévoir aussi les cas où la config désactive l'héritage des logos, et si le logo standard a été supprimé
- Utiliser le logo par défaut des rubriques (à la place de la vieille texture métalique)

## [6.2.5] - 2025-01-29

### 🐛 Bug Fixes

- Include manquant pour la mise à jour via spip-cli

### 📚 Documentation

- Changelog

## [6.2.4] - 2025-01-28

### 🐛 Bug Fixes

- Un code de débug en trop

## [6.2.3] - 2025-01-28

### 🐛 Bug Fixes

- Restaurer le carousel ZoomSur en mode Cognac après le passage en albums
- S'il y a un album, ne pas chercher plus loin pour le carousel ZoomSur
- Warning PHP si pas de logo
- Lien mort, le remplacer par une version équivalente actualisée
- Les logos des mots clefs n'ont pas à avoir de taille minimum
- Le logo en fallback en dûr n'était pas didactique/facile à expliquer ⇒ l'insérer comme logo de rubrique standard s'il n'y en pas déjà un

### 📚 Documentation

- Changelog

## [6.2.2] - 2024-12-10

### 🐛 Bug Fixes

- Un champ de texte libre pour la colonne de droite du pied de page
- Optima est buguée (affichage des accents foireux si profondeur d'imbrication HTML trop grande)
- Compatibilité PHP 7 (pam)

### Build

- V6.2.2

## [6.2.1] - 2024-12-10

### 🐛 Bug Fixes

- N'afficher le lien vers les sites que s'il y en a au moins un à afficher.
- S'il n'y a pas de site à afficher, le signaler
- Un oubli dans la modernisation de la procédure de mise à jour

### Build

- V6.2.1

## [6.2.0] - 2024-12-05

### 🚀 Features

- Utiliser le plugin album pour la galerie de photos en bas des articles
- Une config pour pouvoir masquer les pages des plumes du site

### 🐛 Bug Fixes

- Ne pas afficher dans un Album les images vues dans l'article
- Le plugin Albums est facultatif (pour l'instant)
- Warnings PHP en moins
- MenuFooter doit pouvoir être aussi utilisé sur une Page Unique
- Une vidéo (mp4 local) incluse dans le contenu avec `<embNNN|center>` débordait de la colonne de texte.
- Gérer aussi les numéros au format +33 1 23 45 67 89
- Performances boostées et centrage plus proche du point d'intérêt
- Conserver le fonctionnement avec Portfolio seulement si le plugin Albums n'est pas là
- Compat SPIP 5
- Url du RGAA
- Ajout de label affichables pour Téléphone, Mobile et Fax permettant d'avoir jusqu'à 3 numéros cliquables (Rennes)
- Mise à jour de flipster de 1.1.3 à 1.1.6
- Une fatale si config pas encore enregistrée
- Mise à jour de la lib Vegas de 2.5.4 à 2.6.0
- #SELF peut aussi contenir un `.` : les supprimer aussi
- Mise à jour Fullcalendar de 6.1.11 à 6.1.15

### Build

- Version 6.2.0 + Changelog

## [6.1.0] - 2024-09-09

### 🚀 Features

- Nouveau mot clef : AlaUneRubriques permettant d'afficher des articles à la une d'une rubrique dans une branche
- Mot clef NePasAfficherALaUneRubriques : Affecter ce mot clef aux rubriques qui ne doivent pas afficher les ALaUneRubriques de leur branche
- Une config pour pouvoir afficher la version expansée du contenu d'une rubrique même quand cette dernière a un texte

### 🐛 Bug Fixes

- Léger ajustement du texte affiché sur le cycloshow (petit écran et longue description). Merci Michel !
- Ajustements de la couleur de l'icone ALaUne
- Ne pas souligner les liens dans les rubriques automatiques.

### 📚 Documentation

- Changelog

## [6.0.13] - 2024-08-18

### 🐛 Bug Fixes

- D'autres endroits où il ne faut pas avoir de lien dans les titres
- Il ne faut pas de lien dans les titres dans l'admin de SPIP
- Optimisation de la logique

### Build

- V6.0.13

## [6.0.12] - 2024-08-06

### 🐛 Bug Fixes

- Il ne faut pas de liens dans les titres (Philippe)

### 📚 Documentation

- Changelog
- Changelog
- Changelog

### Build

- V6.0.12

## [6.0.11] - 2024-08-05

### 🐛 Bug Fixes

- Faire le lien sur le mot et non avec un `?` après ce dernier

### 📚 Documentation

- Changelog

### Buid

- 6.0.11

## [6.0.10] - 2024-08-01

### 🚀 Features

- Nouveau mode d'affichage des listes d'article dans une rubrique : en tuiles (comme sur l'accueil en Mode Cognac)
- Pouvoir afficher les ALaUne de la branche dans les rubriques

### 🐛 Bug Fixes

- 2 fichiers en trop
- L'optimisation avait changé la transparence
- Pouvoir choisir explicitement la couleur de soulignement des liens (héritée sinon de la couleur de mise en évidence)
- Rendre accessible les événements affichés en mode Cognac sur la page d'accueil (Christophe)
- Pas de tags html dans un attribut html
- La bonne syntaxe pour afficher ou non le fork de changement de statut en fonction de la version de SPIP

### 📚 Documentation

- Changelog

## [6.0.9] - 2024-06-23

### 🚀 Features

- Possibilité de réinitialiser la config de l'Archivage

### 🐛 Bug Fixes

- Suivre le nouveau formalisme pour les fichiers de langue
- Meilleure compression des images avec https://imgto.xyz/
- Doit rester compatible avec Archivage dans ses futures versions

## [6.0.8] - 2024-06-17

### 🐛 Bug Fixes

- Réintroduire toutes les modifications via un patch
- Un fichier manquant

## [6.0.7] - 2024-06-17

### 🚀 Features

- Utilisation du plugin Archivage de contenu (Merci Éric !)
- Affichage carto GIS sur les mots clefs et les groupes
- Tenir aussi compte des sites référencés (WIP)
- Tenir aussi compte des sites référencés (WIP)
- Quand on déplace un article dans une rubrique archivée, il s'archive automatiquement
- Ajout d'un lien vers l'Ical du site depuis les pages d'agenda
- Ajout d'un lien vers l'Ical du site depuis les pages d'agenda
- Nécessiter le plugin Titres typographiés (améliore la gestion des pictos SCG dans les titres dans l'admin)

### 🐛 Bug Fixes

- Warning PHP si pas de logo du site
- Include prudenciel
- Ne pas tenir compte du statut archivé des rubriques : seuls les articles sont effectivement masqués
- Ne pas tenir compte du statut archivé des rubriques : seuls les articles sont effectivement masqués
- Seuls les articles peuvent être archivés, les rubriques, ont verra plus tard.
- Mieux indiquer le statut Archives des articles, et simplifier la navigation
- Garder la notion qu'on parcours les archives depuis la liste des articles d'une rubrique
- Surcharges du plugin Archivage pour retrouver le comportement précédent de SoyezCréateurs : une rubrique archivée reste visible, pas son contenu (articles)
- Harmonisation des appels aux contenus archivés (grosse simplification des squelettes)
- Simplification de la procédure de transistion depuis le mot clef Archives vers le plugin Archivage
- Sites : afficher le titre de l'article syndiqué + ne pas afficher la date la date de publication selon config SoyezCréateurs
- Encore mieux intégrer les sites référencés dans l'archivage : sites affichés dans la colonne de navigation secondaire
- Passer le mode de navigation archive ici aussi
- Ne pas afficher les rubriques n'ayant que des sites archivés + signaler explicitement que le site est archivé dans son titre
- Les outils secondaires doivent pouvoir afficher un texte cliquable
- Utiliser |attribut_url et |attribut_html dans les modèles
- Pour que ça soit coché dans la config (WIP)
- Le statut archivé des sites référencés doit dépendre du statut de la rubrique en cas de déplacement
- Gérer aussi le cas de la création d'un site référencé
- Tenir compte du statut d'archivage de la rubrique de destination en cas de déplacement d'une rubrique
- Suivre les évolutions du plugin archive_objet
- Finition de l'héritage en cas de déplacement ou création de rubriques
- Les archives sont masquées par défaut, pas besoin d'un critère spécique
- Accessibilité : les liens dans le texte sont soulignés
- Ne pas redéfinir la fonction si déjà présente via le plugin Archivage
- Affichage simplifié et bien formaté des numéro de téléphone dans le pied de page
- Icone SVG inline de la bonne taille partout (c'était trop gros dans En cours de modification)
- L'icone PDF est passée en SVG
- Ne pas imprimer certains éléments de navigation

### 🎨 Styling

- Retours ligne Unix

### ⚙️ Miscellaneous Tasks

- Compatibilité avec le nouveau formulaire de changement de statut de SPIP 4.3

### Build

- V6.0.7

## [6.0.6] - 2024-05-08

### 🚀 Features

- Le formulaire de recherche doit être rempli pour pouvoir être envoyé

### 🐛 Bug Fixes

- Les liens d'évitement doivent être visibles sur mobile.
- Ajustements CSS fullcalendar
- Bien sortir FullCalendar de la compression JS
- Ajustements des CSS du FullCalendar

## [6.0.5] - 2024-04-23

### 🐛 Bug Fixes

- Utiliser Google tag de GA4
- Pas de warning si le plugin n'est pas installé et qu'il n'y a donc pas la table
- Échaper les `[]` pour ne pas les afficher !
- Plus besoin de javascript pour les liens d'évitement
- Le menu de navigation latéral ne se repliait pas  systématiquement avec Firefox (Thierry)

## [6.0.4] - 2024-03-26

### 🐛 Bug Fixes

- Le décalage du contenu par-dessus le logo en bannière devient RWD proportionnellement à la hauteur du logo
- Ajustements des espacements verticaux de l'accueil en Mode Cognac
- Contenir l'effet de zoom sur les carrousel de l'accueil Mode Cognac

### Fix

- Formulaire d'abonnement (avec plugin Mailsubscribers) sans fork et en Ajax

### Build

- V6.0.4

## [6.0.3] - 2024-03-11

### 🚀 Features

- Revert 4fc27e328d787b49d7f3be667210b1183bb29a32 (partiel)
- Le logo d'un site est toujours affiché en haut de la colonne de gauche
- Harmonisation de la présentation des listes dans le Cycloshow en mode Cognac
- Le logo des auteurs n'a pas à avoir une taille minimum
- JQuery Cycle peut être chargé via mes_fonctions.php
- Lien vers le support de formation SPIP + SoyezCréateurs (Olivier)
- Si une rubrique ne contient qu'un seul article, ne renvoyer sur le dit article **que** si la rubrique n'a pas de texte.
- Pouvoir utiliser la v1.0.0 de https://git.spip.net/spip-contrib-extensions/images_compare
- Activer le surlignement des mots cherchés

### 🐛 Bug Fixes

- On avait perdu la transparence sur les éléments de navigation
- La couleur de fond de page concerne aussi la zone de titre (sur et sous-titres)
- Ajustement de la position des élements de la colonne secondaire : en haut !
- La couleur de fond venait d'être perdue sur la page d'accueil en mode Cognac (Gilles)
- Affichage du pied de page en cas d'image de fond
- Ajustements de la page d'accueil en mode cognac quand la navigation est affichée
- Bien cibler seulement le caroussel
- Positionnement du titre si le carousel n'est pas le premier élément sur la page
- Code mort
- Ajustement sur la Couleur  de rubrique sur le bloc titre de la page
- Suivre les évolution de SPIP 4+
- La comparaison d'image doit aussi être fonctionnelle sur mobile/tablette
- Plus besoin du fork de image_compare, il a été intégré.
- Affichage de la 2e image comparée doit avoir la même hauteur que la première (Merci @Rastapopoulos de m'avoir poussé à tester plus)
- Ajustements CSS sur formulaire et chapo...
- Suite Compat SPIP 4 sur des fonctions dépréciées
- Vieilles syntaxes CSS en moins
- Compatibilité avec le plugin Adaptive images

### 🎨 Styling

- Un coup de PHPcbf + and → && ; or → || + valid snake case format

### ⚙️ Miscellaneous Tasks

- Outils pour phpcs (en utilisant les coding standards de SPIP42)
- Un coup de phpStan

### Build

- V6.0.3

## [6.0.2] - 2023-12-27

### 🐛 Bug Fixes

- Les logos affichés dans le titre des articles/rubriques dans les rubriques doivent garder leur taille d'origine (Gilles)
- Compat SPIP 5.0, les `[]` sont plus sensibles
- La bannière de Campagnes doit être dans le header pour ne pas être affichée n'importe où depuis le passage en grid
- Xlink:href est déprécié au profit de href
- Couleur de fond du contenu partout sauf page d'accueil en mode Cognac (y compris sur et sous-titre)
- L'agenda 1/2 hauteur ne fonctionnait plus en mode Cognac
- Apparence des événements d'agenda en page d'accueil du mode Cognac
- Cette règle CSS doit aussi être appliquée sur les configurations anciennes (l'opposé de moderne)
- Cette règle CSS doit aussi être appliquée sur les configurations anciennes (l'opposé de moderne)

### Build

- Version 6.0.2

## [6.0.1] - 2023-11-27

### 🚀 Features

- Effet de zoom sur les images aussi sur les carousel et sur les goodies
- Effet de zoom au survol aussi sur la bandeau Goodies2

### 🐛 Bug Fixes

- Cette règle CSS mettait un grand espace inutile sous l'ours
- Rainette a besoin de la langue en paramètre
- Ajustements de la présentation de la météo (avec des CSS modernes)
- Sans le plugin Noizetier, le cycloshow était énooooooooorme !

### 🎨 Styling

- Fin de lignes Unix

### Build

- On sera finalement juste compatible avec SPIP 4.2 (en forçant minimum 4.2.6)

## [5.2.48] - 2023-09-04

### Change

- Optimisation des webperf sur les icones SVG

## [5.2.46] - 2023-06-27

### 🐛 Bug Fixes

- Meilleure présentation du mode en travaux.

## [5.2.42] - 2022-11-24

### 🐛 Bug Fixes

- Retour en haut fonctionnel aussi sur la page d'accueil

## [5.2.40] - 2022-09-13

### Remove

- Cette meta n'est plus utilisée depuis longtemps (plugin datant de SPIP 1.9)

## [5.0.99] - 2020-03-26

### JQMIGRATE

- JQuery.fn.load() is deprecated (compatibilité SPIP 3.2/jQuery 3.0)

### Http

- //yellowlab.tools/ invite à utiliser la notion d'event delegation avec jQuery.
- //bassistance.de/jquery-plugins/jquery-plugin-tooltip/ n'est plus supporté, passons à http://jqueryui.com/tooltip/

### Og

- Locale doit indiquer la langue et le PAYS (merci erational)

<!-- generated by git-cliff -->
