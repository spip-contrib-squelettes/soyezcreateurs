<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

include_spip('inc/pipelines');
include_spip('gis_pipelines');
include_spip('gisgeom_pipelines');
