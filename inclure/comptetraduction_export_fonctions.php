<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

require_once find_in_path('lib/Spout/Autoloader/autoload.php');

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Common\Entity\Row;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;

function trad_export_csv($trads) {

	$entetes = [
		'ID',
		'Nom',
		'URL',
		'Contenu'
	];

	$default_style = (new StyleBuilder())
		->setFontName('Arial')
		->setFontSize(11)
		->build();

	$writer = WriterEntityFactory::createXLSXWriter(); // for XLSX files
	$writer->setTempFolder(_DIR_RACINE . _NOM_TEMPORAIRES_INACCESSIBLES);
	$writer->setDefaultRowStyle($default_style);

	$writer->openToBrowser('traduction.xlsx'); // stream data directly to the browser

	$row_style_bold = (new StyleBuilder())
		->setFontBold()
		->build();

	$writer->addRow(WriterEntityFactory::createRowFromArray($entetes, $row_style_bold)); // add a row at a time

	foreach ($trads as $trad) {
		$writer->addRow(WriterEntityFactory::createRowFromArray($trad)); // add a row at a time
	}

	$writer->close();
}
