<?php

/***************************************************************************\
 *  SPIP, Système de publication pour l'internet                           *
 *                                                                         *
 *  Copyright © avec tendresse depuis 2001                                 *
 *  Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribué sous licence GNU/GPL.     *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function soyezcreateurs_declarer_tables_objets_sql($tables) {
	$tables['spip_syndic']['table_objet_surnoms'] =  ['site', 'refsite'];
	$tables['spip_syndic']['type_surnoms'] =  ['syndic', 'refsite'];
	return $tables;
}
