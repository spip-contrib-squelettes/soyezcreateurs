<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

// Permet de recadrer une image en la centrant sur son focus (plugin Centre Image)
function image_focus($img, $largeur, $hauteur, $position = 'center') {
	if (!$img) { return('');
	}

	$ext = pathinfo(supprimer_timestamp(extraire_attribut($img, 'src')), PATHINFO_EXTENSION);
	if ($ext === 'svg') {
		return $img;
	}

	$largeurimg = largeur($img);
	$hauteurimg = hauteur($img);

	$GLOBALS['Smush_Debraye'] = true;

	if (($largeurimg <= $largeur) && ($hauteurimg <= $hauteur)) {
		$img = filtrer('image_recadre', $img, $largeur, $hauteur, $position, 'transparent');
	} elseif (($largeurimg <= $largeur) || ($hauteurimg <= $hauteur)) {
		if ($largeurimg <= $largeur) {
			$img = filtrer('image_recadre', $img, "$largeurimg:$hauteur", '-', 'focus-center', 'transparent');
			$img = filtrer('image_graver', $img);
		} else {
			$img = filtrer('image_recadre', $img, "$largeur:$hauteurimg", '-', 'focus-center', 'transparent');
			$img = filtrer('image_graver', $img);
		}
		$img = filtrer('image_recadre', $img, $largeur, $hauteur, $position, 'transparent');
	} else {
		$img = filtrer('image_recadre', $img, "$largeur:$hauteur", '-', 'focus-center', 'transparent');
		$img = filtrer('image_graver', $img);
		$img = filtrer('image_reduire', $img, $largeur);
	}

	$GLOBALS['Smush_Debraye'] = false;

	return $img;
}
