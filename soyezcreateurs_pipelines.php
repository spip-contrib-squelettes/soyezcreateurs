<?php

/*
* Pipeline pour SoyezCréateurs
* Realisation : RealET : real3t@gmail.com
* Attention, fichier en UTF-8 sans BOM
*/

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function soyezcreateurs_jqueryui_plugins($scripts) {
	if (lire_config('soyezcreateurs/native_tooltips') !== 'on') {
		$scripts[] = 'jquery.ui.tooltip';
	}
	return $scripts;
}

function soyezcreateurs_formulaire_traiter($flux) {
	if (strpos($flux['args']['form'], 'configurer_soyezcreateurs') !== false) {
		include_spip('inc/invalideur');
		purger_repertoire(_DIR_VAR . 'cache-css');
		purger_repertoire(_DIR_VAR . 'cache-js');
		suivre_invalideur('configurer_soyezcreateurs');
	}
	return $flux;
}

function soyezcreateurs_noizetier_blocs_defaut($flux) {
	$flux = ['contenu'];
	return $flux;
}

function soyezcreateurs_identite_extra_champs($champs) {
	$champs = explode(',', _CHAMPS_INDENTITE_EXTRA_SC);
	$champs[] = 'youtube';
	$champs[] = 'facebook';
	$champs[] = 'twitter';
	$champs[] = 'mastodon';
	$champs[] = 'linkedin';
	$champs[] = 'instagram';
	$champs[] = 'pinterest';
	$champs[] = 'viadeo';
	$champs[] = 'gitea';
	$champs[] = 'github';
	$champs[] = 'spip';
	return $champs;
}

/*
  S'il y a un Cookie de lang ({lang}, on va chercher une image :
  - de même extension que le logo du site ({extention})
  - dans le dossier images/logo/
  - portant le nom site_{lang}.{extension}
*/
function soyezcreateurs_quete_logo_objet($flux) {
	if (
		isset($_COOKIE['spip_lang'])
		&& !empty($flux['data'])
		&& $flux['args']['objet'] === 'site'
		&& intval($flux['args']['id_objet']) === 0
		&& $flux['args']['mode'] !== 'off'
	) {
		$lang = $_COOKIE['spip_lang'];
		$extension = pathinfo($flux['data']['chemin'], PATHINFO_EXTENSION);
		if ($image = find_in_path('images/logo/site_' . $lang . '.' . $extension)) {
			$flux['data'] = [
				'chemin'    => $image,
				'timestamp' => @filemtime($image),
			];
		}
	}

	return $flux;
}

/*
  Surcharge du plugin Archivage contenu :
  Ne pas masquer les rubriques ayant le statut archivé
  Sur une inspiration de https://blog.eliaz.fr/article114.html
*/
function soyezcreateurs_pre_boucle(Boucle $boucle) : Boucle {
	if ($boucle->type_requete === 'rubriques' && !isset($boucle->modificateur['ignorer_archivage'])) {
		$boucle->modificateur['ignorer_archivage'] = true;
	}
	return $boucle;
}

function soyezcreateurs_post_edition($flux) {
	// Cas des rubriques : création
	if (isset($flux['args']['table']) && $flux['args']['table'] === 'spip_rubriques'
		&& isset($flux['args']['action']) && $flux['args']['action'] === 'modifier'
		&& isset($flux['args']['id_objet'])
		&& isset($flux['args']['champs_anciens'])
		&& isset($flux['args']['champs_anciens']['id_rubrique'])
		&& isset($flux['args']['champs_anciens']['id_parent'])
	) {
		// Trouver la rubrique parente, et le statut archivé de la rubrique en cours
		$id_parent = sql_getfetsel(
			'id_parent',
			'spip_rubriques',
			'id_rubrique=' . intval($flux['args']['id_objet'])
		);

		$res = sql_fetsel(
			['est_archive', 'objet_racine_archive', 'id_racine_archive'],
			'spip_rubriques',
			'id_rubrique=' . $id_parent
		);
		$est_archive_rubrique = $res['est_archive'] ?? '0';
		$objet_racine_archive = $res['objet_racine_archive'] ?? '';
		$id_racine_archive = $res['id_racine_archive'] ?? '0';

		if ($est_archive_rubrique == '1') {
			if (intval($id_racine_archive) > 0) {
				// La rubrique parente a une racine d'archive
				// Il faut commencer par désarchiver parce que la fonction archivage_objet_modifier_enfants n'est pas réentrante
				archivage_objet_modifier('desarchiver', 'rubrique', $id_racine_archive, 'archive_defaut');
				archivage_objet_modifier('archiver', 'rubrique', $id_racine_archive, 'archive_defaut');
			} else {
				// La rubrique parente est la racine d'archive
				// Il faut commencer par désarchiver parce que la fonction archivage_objet_modifier_enfants n'est pas réentrante
				archivage_objet_modifier('desarchiver', 'rubrique', $id_parent, 'archive_defaut');
				archivage_objet_modifier('archiver', 'rubrique', $id_parent, 'archive_defaut');
			}
		}
	}
	// Cas des articles
	if (isset($flux['args']['table']) && $flux['args']['table'] === 'spip_articles'
		&& isset($flux['args']['action']) && $flux['args']['action'] === 'instituer'
		&& isset($flux['args']['id_objet'])
		&& isset($flux['data']['id_rubrique'])
	) {
		$res = sql_fetsel(
			['est_archive', 'objet_racine_archive', 'id_racine_archive'],
			'spip_rubriques',
			'id_rubrique=' . intval($flux['data']['id_rubrique'])
		);
		$est_archive_rubrique = $res['est_archive'] ?? '0';
		$objet_racine_archive = $res['objet_racine_archive'] ?? '';
		$id_racine_archive = $res['id_racine_archive'] ?? '0';

		if ($est_archive_rubrique == '1') {
			archivage_objet_modifier('archiver', 'article', $flux['args']['id_objet'], 'archive_defaut');
			sql_updateq('spip_articles', [ 'objet_racine_archive' => $objet_racine_archive ?? 'rubrique', 'id_racine_archive' => $id_racine_archive ?? $flux['data']['id_rubrique'] ], 'id_article=' . $flux['args']['id_objet']);
		} else {
			$est_archive_article = sql_fetsel('est_archive', 'spip_articles', 'id_article=' . $flux['args']['id_objet']);
			$est_archive_article = array_shift($est_archive_article);
			if ($est_archive_article == '1') {
				archivage_objet_modifier('archiver', 'article', $flux['args']['id_objet'], 'archive_defaut');
			}
		}
	}

	// Cas des sites réféncés : déplacement
	if (isset($flux['args']['table']) && $flux['args']['table'] === 'spip_syndic'
		&& isset($flux['args']['action']) && $flux['args']['action'] === 'instituer'
		&& isset($flux['args']['id_objet'])
		&& isset($flux['args']['id_parent_ancien'])
		&& isset($flux['data']['id_rubrique'])
		&& ($flux['args']['id_parent_ancien'] !== $flux['data']['id_rubrique'])
	) {
		$res = sql_fetsel(
			['est_archive', 'id_racine_archive'],
			'spip_rubriques',
			'id_rubrique=' . intval($flux['data']['id_rubrique'])
		);
		$est_archive_rubrique = $res['est_archive'] ?? '0';
		$id_racine_archive = $res['id_racine_archive'] ?? '0';

		if ($est_archive_rubrique == '1') {
			$objet_racine_archive = 'rubrique';
			if ($id_racine_archive === '0') {
				$id_racine_archive = intval($flux['data']['id_rubrique']);
			}
			archivage_objet_modifier('archiver', 'syndic', $flux['args']['id_objet'], 'archive_defaut');
			sql_updateq('spip_syndic', [ 'objet_racine_archive' => $objet_racine_archive ?? 'rubrique', 'id_racine_archive' => $id_racine_archive ?? $flux['data']['id_rubrique'] ], 'id_syndic=' . $flux['args']['id_objet']);
		} else {
			$est_archive_article = sql_fetsel('est_archive', 'spip_syndic', 'id_syndic=' . $flux['args']['id_objet']);
			$est_archive_article = array_shift($est_archive_article);
			if ($est_archive_article == '1') {
				archivage_objet_modifier('archiver', 'syndic', $flux['args']['id_objet'], 'archive_defaut');
			}
		}
	}
	// Cas des sites réféncés : création
	if (isset($flux['args']['table']) && $flux['args']['table'] === 'spip_syndic'
		&& isset($flux['args']['action']) && $flux['args']['action'] === 'modifier'
		&& isset($flux['args']['id_objet'])
		&& isset($flux['args']['champs_anciens'])
		&& isset($flux['args']['champs_anciens']['id_rubrique'])
		&& !isset($flux['data']['est_archive'])
	) {
		$res = sql_fetsel(
			['est_archive', 'id_racine_archive'],
			'spip_rubriques',
			'id_rubrique=' . intval($flux['args']['champs_anciens']['id_rubrique'])
		);
		$est_archive_rubrique = $res['est_archive'] ?? '0';
		$id_racine_archive = $res['id_racine_archive'] ?? '0';

		if ($est_archive_rubrique == '1') {
			$objet_racine_archive = 'rubrique';
			if ($id_racine_archive === '0') {
				$id_racine_archive = intval($flux['args']['champs_anciens']['id_rubrique']);
			}
			archivage_objet_modifier('archiver', 'syndic', $flux['args']['id_objet'], 'archive_defaut');
			sql_updateq('spip_syndic', [ 'objet_racine_archive' => $objet_racine_archive ?? 'rubrique', 'id_racine_archive' => $id_racine_archive ?? $flux['args']['champs_anciens']['id_rubrique'] ], 'id_syndic=' . $flux['args']['id_objet']);
		} else {
			$est_archive_article = sql_fetsel('est_archive', 'spip_syndic', 'id_syndic=' . $flux['args']['id_objet']);
			$est_archive_article = array_shift($est_archive_article);
			if ($est_archive_article == '1') {
				archivage_objet_modifier('archiver', 'syndic', $flux['args']['id_objet'], 'archive_defaut');
			}
		}
	}
	return $flux;
}

// Pour le déplacement d'une rubrique (pas de post_edition dans SPIP ici)
function soyezcreateurs_notifications($flux) {
	if (isset($flux['args']['quoi']) && $flux['args']['quoi'] === 'rubrique_instituer'
		&& isset($flux['args']['id'])
		&& isset($flux['args']['options'])
		&& isset($flux['args']['options']['id_parent_ancien'])
		&& ($flux['args']['id'] !== $flux['args']['options']['id_parent_ancien'])
	) {
		// Trouver la rubrique parente, et le statut archivé de la rubrique en cours
		$currentrub = sql_fetsel(
			['id_parent', 'est_archive'],
			'spip_rubriques',
			'id_rubrique=' . intval($flux['args']['id'])
		);
		$id_parent = $currentrub['id_parent'] ?? '0';
		$est_archive_currentrub = $currentrub['est_archive'] ?? '0';
		if ($id_parent > 0) {
			// Est-ce que la rubrique parente est archivée
			$rubparente = sql_fetsel(
				['est_archive', 'id_racine_archive'],
				'spip_rubriques',
				'id_rubrique=' . $id_parent
			);
			$est_archive_rubrique_parente = $rubparente['est_archive'] ?? '0';
			$id_racine_archive = $rubparente['id_racine_archive'] ?? '0';
			if ($est_archive_rubrique_parente == '1') {
				if (intval($id_racine_archive) > 0) {
					// La rubrique parente a une racine d'archive
					// Il faut commencer par désarchiver parce que la fonction archivage_objet_modifier_enfants n'est pas réentrante
					archivage_objet_modifier('desarchiver', 'rubrique', intval($flux['args']['id']), 'archive_defaut');
					archivage_objet_modifier('desarchiver', 'rubrique', $id_racine_archive, 'archive_defaut');
					archivage_objet_modifier('archiver', 'rubrique', $id_racine_archive, 'archive_defaut');
					return $flux;
				} else {
					// La rubrique parente est la racine d'archive
					// Il faut commencer par désarchiver parce que la fonction archivage_objet_modifier_enfants n'est pas réentrante
					archivage_objet_modifier('desarchiver', 'rubrique', intval($flux['args']['id']), 'archive_defaut');
					archivage_objet_modifier('desarchiver', 'rubrique', $id_parent, 'archive_defaut');
					archivage_objet_modifier('archiver', 'rubrique', $id_parent, 'archive_defaut');
					return $flux;
				}
			}
		}
		if ($est_archive_currentrub == '1') {
			// La rubrique en cours devient une racine d'archivage
			// Il faut commencer par désarchiver parce que la fonction archivage_objet_modifier_enfants n'est pas réentrante
			archivage_objet_modifier('desarchiver', 'rubrique', $flux['args']['id'], 'archive_defaut');
			archivage_objet_modifier('archiver', 'rubrique', $flux['args']['id'], 'archive_defaut');
		}
	}
	return $flux;
}

function soyezcreateurs_archivage_exclusion_tables($flux) {
	// Exclure de l'archivage sur toutes ces tables
	$flux['data'][] = 'spip_evenements';
	$flux['data'][] = 'spip_auteurs';
	$flux['data'][] = 'spip_forum';
	$flux['data'][] = 'spip_mots';
	$flux['data'][] = 'spip_gis';
	$flux['data'][] = 'spip_documents';
	$flux['data'][] = 'spip_dictionnaires';
	$flux['data'][] = 'spip_definitions';
	$flux['data'][] = 'spip_groupes_mots';
	$flux['data'][] = 'spip_syndic_articles';
	$flux['data'][] = 'spip_almanachs';
	$flux['data'][] = 'spip_depots';
	$flux['data'][] = 'spip_paquets';
	$flux['data'][] = 'spip_plugins';

	return $flux;
}
