<?php

/*
* Configuration de Noizetier pour SoyezCréateurs
* Realisation : RealET : real3t@gmail.com
*/

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

include_spip('base/soyezcreateurs');

/*
	Fonction pour changer les mots liés à un article :
	- changer un mot pour un autre
	- inverser 2 mots
*/
function sc_mig_mot($mot_source, $gp_source, $mot_dest, $gp_dest, $inverser = false) {
	$id_mot_source = id_mot($mot_source, id_groupe($gp_source));
	$id_mot_dest = id_mot($mot_dest, id_groupe($gp_dest));
	// Trouver les articles attachés à EDITO et ZoomSur
	$articles_source = sql_allfetsel('id_objet', 'spip_mots_liens', "id_mot=$id_mot_source AND objet='article'");
	if ($inverser) {
		$articles_dest = sql_allfetsel('id_objet', 'spip_mots_liens', "id_mot=$id_mot_dest AND objet='article'");
	}
	if ($articles_source) {
		foreach ($articles_source as $article_source) {
			create_lien_mot($id_mot_dest, $article_source['id_objet'], 'article');
			delete_lien_mot($id_mot_source, $article_source['id_objet'], 'article');
		}
	}
	if ($inverser) {
		if ($articles_dest) {
			foreach ($articles_dest as $article_dest) {
				create_lien_mot($id_mot_source, $article_dest['id_objet'], 'article');
				delete_lien_mot($id_mot_dest, $article_dest['id_objet'], 'article');
			}
		}
	}
}
