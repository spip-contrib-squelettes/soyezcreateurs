<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function offset_time_zone($timezone, $town) {
	date_default_timezone_set('Europe/Paris');
	$daylight_savings_offset_in_seconds_in_france = timezone_offset_get(timezone_open('Europe/Paris'), new DateTime());
	$daylight_savings_offset_in_seconds = timezone_offset_get(timezone_open($timezone), new DateTime());
	$return = ($daylight_savings_offset_in_seconds - $daylight_savings_offset_in_seconds_in_france + 3600) / 3600 . ';' . $town;
	return $return;
}
