<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
// Modification de l'interface d'admin
'info_descriptif' => 'Bulle d’aide :',
'texte_descriptif_rapide' => 'Bulle d’aide <i>(et chapeau si le chapeau vide)</i> + Meta Description <i>(pour les moteurs de recherche. Si vide, replis sur la Bulle d’aide de la rubrique mère, sinon la description du site)</i>',
'telephonedesc'	=> 'Label téléphone',
'telephone'	=> 'Téléphone <i>(format +33.123456789)</i>',
'mobiledesc'	=> 'Label Mobile',
'mobile'	=> 'Mobile <i>(format +33.123456789)</i>',
'faxdesc'	=> 'Label Fax',
'fax'	=> 'Fax <i>(format +33.123456789)</i>',
'email_contact'	=> 'Email de contact',
'informations'	=> 'Informations',
'id_article_contact'	=> 'ID article de contact',
'id_image_pied'	=> 'ID image pied',
'lien_image_pied'	=> 'URL du lien sur l’image',
'id_formidable'	=> 'ID formulaire',
'textecol2'	=> 'Texte libre en 2ᵉ colonne du pied',
'latitude'	=> 'Latitude',
'longitude'	=> 'Longitude',
'id_gis'	=> 'ID carte GIS',
'facebook'	=> 'Facebook',
'twitter'	=> 'Twitter',
'mastodon'	=> 'Mastodon',
'linkedin'	=> 'LinkedIn',
'instagram'	=> 'Instagram',
'pinterest'	=> 'Pinterest',
'viadeo'	=> 'Viadeo',
'youtube'	=> 'Youtube',
'spip'	=> 'SPIP contrib',
'gitea'	=> 'Git.spip.net',
'github'	=> 'GitHub',

];
