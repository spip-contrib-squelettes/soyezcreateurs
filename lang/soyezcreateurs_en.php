<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/soyezcreateurs?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// A
	'accessibilite_menu' => 'Go to menu',
	'accessibilite_onglets' => 'Go to tabs',
	'accessibilite_recherche' => 'Go to Search box',
	'accessibilite_texte' => 'Go to text',
	'accueil' => 'Home',
	'accueil_site' => 'Back to Home of the site',
	'actualite_toutes' => 'Latest articles',
	'actualite_toutes_arbo' => 'list',
	'actualite_toutes_blocs' => 'visuals',
	'agenda' => 'Calendar',
	'agenda_exporter_ical' => 'Export in iCal format',
	'agenda_fsd' => 'From @djour_l@ @djour@ @dmois_l@ @dannee@ at @dheure@:@dminutes@ to the  @fjour_l@ @fjour@ @fmois_l@ @fannee@ at @fheure@:@fminutes@',
	'agenda_fsd_notime' => 'From @djour_l@ @djour@ @dmois_l@ @dannee@ to the @fjour_l@ @fjour@ @fmois_l@ @fannee@',
	'agenda_proposer_evenement' => 'Submit an event',
	'agenda_proposer_evenement_explication' => 'You may suggest an event to the site administrator who will decide whether or not to publish it. Your event has more chance of being published if it has as much detail as possible.',
	'agenda_proposer_evenement_title' => 'You may suggest an event to the site’s administrator.',
	'agenda_sd' => '@djour_l@ @djour@ @dmois_l@ @dannee@ from @dheure@:@dminutes@ to @fheure@:@fminutes@',
	'agenda_sd_h' => '@djour_l@ @djour@ @dmois_l@ @dannee@ at @dheure@:@dminutes@',
	'agenda_sd_notime' => '@djour_l@ @djour@ @dmois_l@ @dannee@',
	'agenda_sinscrire' => 'Subscribe to the event',
	'agendamoisde' => 'Calendar for the month',
	'aidesc' => 'Help about SoyezCréateurs',
	'alaune' => 'Featured',
	'annuaire' => 'Directory',
	'annuaire_consulter' => 'Consult the directory',
	'annuaire_local' => 'Local directory',
	'annuaire_vide' => 'The directory is empty.',
	'archives' => 'Archives',
	'archives_title' => 'View the archived articles',
	'articlesconnexes' => 'Related articles',
	'auteur' => 'Author',
	'auteurs_liste' => 'List of authors',
	'auteurs_site' => 'Site authors',

	// C
	'carte_monde' => 'World map',
	'chapitre_complet' => 'Display complete chapter for printing',
	'chapitre_complet_title' => 'Display the entire section contents: ',
	'chercheravecgoogle' => 'Search the Web with <a href=\'http://www.google.com/\'><img src=\'http://www.google.com/logos/Logo_25wht.gif\' alt=\'Google\' title=\'Google\' style=\'vertical-align:middle;\' width=\'75\' height=\'32\' /></a> !',
	'clever_uns' => 'Unsubscription done',
	'clevermail' => 'CleverMail',
	'commencer' => 'To start',
	'configurersc' => 'Set SoyezCréateurs',
	'connexion' => 'Connection',
	'connexiontitle' => 'Access to admin interface',
	'copyright_cnil' => 'CNIL declaration nº',
	'copyright_realisation' => 'Made by: ',
	'copyright_spip' => 'Skeleton <a href=\'https://www.pyrat.net/\' title=\'Visit the website of the creator of this skeleton\'>SoyezCréateurs</a> powered by <a href=\'https://www.spip.net/\' title=\'Visit the SPIP website, Content Management System in GPL license\'>SPIP</a>',

	// D
	'deconnexion' => 'Logout',
	'deconnexiontitle' => 'Logout',
	'deposer_intention' => 'Send a prayer intention',
	'derniereactualites' => 'Latest news',
	'dernieremaj' => 'Last update of the site:',
	'derniersarticlespublies' => 'The last articles published',
	'descriptif' => 'Description',
	'discus_en_cours' => 'Ongoing discussion',
	'docatelecharger' => 'Document download',

	// E
	'ecoute' => 'Listen to :',
	'editos' => 'Editorials',
	'erreur' => 'Error !',
	'erreur_documentexistepas' => 'Error : this document does not exist!',
	'erreur_excuses_404' => 'Sorry…',
	'erreur_excuses_404_explications' => '<p>The page that you’re looking for couldn’t be located because :</p><ol class="spip"><li>it is no longer up-to-date,</li><li>the link pointing to the page is obsolete,</li><li>or the referenced page has been moved.</li></ol><p>Please try and find your information through the <a href="@urlsite@">Home page: @nomsite@</a>, or by searching for a keyword on the form below.</p>',

	// F
	'forum' => 'Forum',
	'forum_enreponse' => 'In reply to:',
	'forum_enreponse_breve' => 'In reply to the item:',
	'forum_enreponse_message' => 'In reply to the message:',
	'forum_prenom' => 'Who are you?',
	'forum_repondre' => 'Add your testimony',
	'forum_vosreponses' => 'Your testimonies',
	'forum_votre_email_explication' => 'Your email address (<strong>This will not be made public on the site,</strong>, but is necessary in order to send replies to you)',
	'forum_votre_prenom' => 'Your firstname:',

	// L
	'liresuite' => 'Read more',
	'liresuitede' => 'More to read about :',
	'liresuiteeditorial' => 'Read the end of the editorial',
	'liresuitesyndic' => 'Read more on the original site…',
	'listezones' => 'Hot spot’s list',

	// M
	'memerubrique' => 'In the same section…',
	'mentions_legales_obligatoires' => 'French compulsory legal notices ([CNIL|Commision Nationale Informatique et Liberté->http://www.cnil.fr/] and [LcEN|Loi sur la confiance en l’économie Numérique->http://www.legifrance.gouv.fr/WAspad/UnTexteDeJorf?numjo=ECOX0200175L]). Everything that [you need to know->https://www.economie.gouv.fr/entreprises/site-internet-mentions-obligatoires]. [Decrypting the legal obligations->https://www.maitre-eolas.fr/post/2008/03/24/905-blogueurs-et-responsabilite-reloaded].',
	'menu_deplier' => 'Expand: ',
	'menu_picalt' => 'Click to ',
	'menu_replier' => 'Collapse: ',
	'menunavrwd' => 'Navigation menu',
	'mot' => 'Word',
	'mots' => 'Words',
	'mots_title' => 'The site’s keywords',
	'motsgroupe' => 'Group of Words',
	'multimedia' => 'Listen',
	'multimedia_title' => 'Listen to or watch this clip (opens a new window)',

	// N
	'newsletter' => 'Site newsletter',
	'newsletter_consulter' => 'View the old newsletters',
	'newsletter_recevoir' => 'Receive the site newsletter',

	// O
	'ordreantichronologique' => 'reverse chronological order',
	'ou_sommes_nous' => 'Locate us on the map!',

	// P
	'par' => 'by',
	'participez' => 'Take part in the life of the site!',
	'plan_menu' => 'Sitemap',
	'plus_loin' => 'See also',
	'plus_loin_title' => 'Reference site',
	'precisezrecherche' => 'Refine your search',
	'publiele' => 'Published on',

	// Q
	'quoideneuf' => 'What’s new?',

	// R
	'recherche_infructueuse' => 'Nothing found.',
	'recherche_label' => 'Search this site',
	'recherche_title' => 'Please enter your search',
	'recherche_title_ok' => 'Begin search',
	'recherche_total' => 'Total items found',
	'recherche_value' => 'Search ?',
	'retoursommaire' => 'Back to Overview',
	'retourtop' => 'Back to top',
	'rss_abonnement' => 'Simply copy the following URL into your aggregator:',
	'rss_abonnement_titre' => 'Subscribe',
	'rss_abonnement_titre_page' => 'Subscribe to :',
	'rss_explication' => 'RSS feed is a free site news summary. It provides content or summaries of content, together with links to the full versions. The last published items may then be read by your favorite RSS <a href="http://en.wikipedia.org/wiki/Aggregator">aggregator</a>.',
	'rss_explication_titre' => 'What is an RSS feed?',
	'rubrique_securisee' => 'Open reserved access',

	// S
	'savoirplus' => 'More…',
	'savoirpluscritere' => 'Learn more about the criterion',
	'sedna' => 'Sedna (RSS aggregator)',
	'sommaire' => 'Summary',
	'syndiquer_agenda' => 'Syndiquer l’agenda',

	// T
	'themes' => 'Themes',
	'tousarticlesantichrono' => 'All articles by date',
	'tout' => '(no filter)',
	'toutleplan' => 'The entire site on one page',

	// V
	'voirarticlespar' => 'See the articles by',
	'voirdetailannee' => 'See the year’s details',
	'voirdetailmois' => 'See the details for the month of',
	'voirimage' => 'See the image on its own',
	'voirle' => 'See the',
	'voirsitespar' => 'See the sites with',
	'vous_souhaitez_etre_tenu_au_courant' => 'You want to be kept informed?',
];
