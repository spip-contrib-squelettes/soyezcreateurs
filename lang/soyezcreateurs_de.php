<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/soyezcreateurs?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// A
	'accessibilite_menu' => 'Gehe zur Menu',
	'accessibilite_onglets' => 'Gehe zur Karteireiter',
	'accessibilite_recherche' => 'Suchen',
	'accessibilite_texte' => 'Gehe zur Text',
	'accueil' => 'Startseite',
	'accueil_site' => 'Zurueck zur Startseite',
	'agenda' => 'Kalendar',
	'agenda_exporter_ical' => 'Exportieren ins iCal Format',
	'agenda_proposer_evenement' => 'Ein Aenderung vorschlagen',
	'agenda_proposer_evenement_explication' => 'Sie koennen einen Vorschlag an die Verwaltung hier machen. Um anerkannt zu sein sollen die Aenderungen so detailiert sein wie moeglich.',
	'agenda_proposer_evenement_title' => 'Sie koennen ein Aenderung an die Verwaltung hier vorschlagen',
	'agenda_sinscrire' => 'schreiben in Ereignisse',
	'agendamoisde' => 'Monats Kalender von',
	'aidesc' => 'Hilfe bei SoyezCréateurs',
	'alaune' => 'Zum Anfang',
	'annuaire' => 'Verzeichnis',
	'annuaire_consulter' => 'Kalendar anschauen',
	'annuaire_local' => 'Umgebungs Kalendar',
	'annuaire_vide' => 'Verzeichnis ist leer',
	'archives' => 'Archiven',
	'archives_title' => 'Artikle Archiv anschauen',
	'articlesconnexes' => 'Gelesenen artikel ',
	'auteur' => 'Verfasser ',
	'auteurs_liste' => 'Liste der Verfassers',
	'auteurs_site' => 'Site Verfassers',

	// C
	'carte_monde' => 'Welt Karte',
	'chapitre_complet' => 'Vollständige Kapitel für den Druck',
	'chapitre_complet_title' => 'Gesamte Anzeigen des Inhalts des Themas:',
	'chercheravecgoogle' => 'mit google suchen',
	'clever_uns' => 'Abmeldung abgeschlossen',
	'clevermail' => 'CleverMail',
	'commencer' => 'Für einen guten start',
	'configurersc' => 'Konfigurieren von Soyezcreateurs',
	'connexion' => 'Verbindung',
	'connexiontitle' => 'Zugang zum reservierten Bereich',
	'copyright_cnil' => 'Erklärung CNIL Nr.',
	'copyright_realisation' => 'Regie:',
	'copyright_spip' => 'Skelett <a href \'https://www.pyrat.net/\' title \'Visiter le site du créateur de ce squelette\'> Soyezcreateurs</a> angetrieben von <a href \'http://www.spip.net/\' title \'Visiter le site de SPIP, logiciel de gestion de contenu web en licence libre GPL\'> SPIP</a>',

	// D
	'deconnexion' => 'Logout',
	'deconnexiontitle' => 'Trennen Sie',
	'deposer_intention' => 'Ein Gebetsanliegen senden',
	'derniereactualites' => 'Aktuelle news',
	'dernieremaj' => 'Letztes Update der Website:',
	'derniersarticlespublies' => 'Die neuesten Artikel veröffentlicht',
	'descriptif' => 'Übersicht',
	'discus_en_cours' => 'Die aktuelle Diskussion',
	'docatelecharger' => 'Dokumente zum Herunterladen',

	// E
	'ecoute' => 'Anhören:',
	'editos' => 'Leitartikel',
	'erreur' => 'Fehler!',
	'erreur_documentexistepas' => 'Fehler: Dieser Dokument existiert nicht!',
	'erreur_excuses_404' => 'Es tut uns leid...',
	'erreur_excuses_404_explications' => '<p>Die Seite, die Sie suchen konnte gefunden werden, weil:</p><ol class="spip"><li>es war aktueller,</li> <li>der Link zu der Seite, die du suchst ist veraltet,</li> <li>die angeforderte Seite wurde verschoben.</li></ol><p>Wir danken Ihnen, beginnen Ihre Suche auf der <a href \'@urlsite@\'> Homepage: @nomsite @</a>, oder suchen Sie nach einem Stichwort mit dem Formular unten.</p>',

	// F
	'forum' => 'Forum',
	'forum_enreponse' => 'Antwort auf den Artikel:',
	'forum_enreponse_breve' => 'Antwort auf den Brief:',
	'forum_enreponse_message' => 'Als Reaktion auf die Nachricht:',
	'forum_prenom' => 'Wer sind Sie?',
	'forum_repondre' => 'Fügen Sie Ihre Empfehlung',
	'forum_vosreponses' => 'Ihre Zeugnisse',
	'forum_votre_prenom' => 'Ihre Vorname:',

	// L
	'liresuitede' => 'Lesen Sie mehr von:',
	'liresuiteeditorial' => 'Lesen Sie den Rest der Redaktion',
	'liresuitesyndic' => 'Lesen Sie mehr auf der ursprünglichen Website...',
	'listezones' => 'Liste der klickbare Bereiche',

	// M
	'memerubrique' => 'In der gleichen Rubrik...',
	'mentions_legales_obligatoires' => 'Obligatorische rechtlicher Hinweis ([CNIL|Der Kommission Nationale Informatique et Liberté->http://www.cnil.fr/] und [LcEN|Recht auf Vertrauen in die digitale Wirtschaft->http://www.legifrance.gouv.fr/WAspad/UnTexteDeJorf?numjo ECOX0200175L]). Alles [Sie wissen müssen->https://www.economie.gouv.fr/entreprises/site-internet-mentions-obligatoires]. [Entschlüsselung der rechtlichen Verpflichtungen->http://www.maitre-eolas.fr/post/2008/03/24/905-blogueurs-et-responsabilite-reloaded].',
	'menu_deplier' => 'd\\351 entfalten:',
	'menu_picalt' => 'Klicken Sie auf',
	'menu_replier' => 'Falten:',
	'menunavrwd' => 'Navigations-Menü',
	'mot' => 'Wort',
	'mots' => 'Wörter',
	'mots_title' => 'Website tags',
	'motsgroupe' => 'Gruppe von Wörtern',
	'multimedia' => 'Anhören',
	'multimedia_title' => 'Anhören oder Ansehen des Extrakts (öffnet ein neues Fenster)',

	// N
	'newsletter' => 'Website NewsLetter',
	'newsletter_recevoir' => 'Website Newsletter Abonnieren',

	// O
	'ordreantichronologique' => 'Gegenuhrzeigersinn',
	'ou_sommes_nous' => 'So finden Sie uns auf der Karte!',

	// P
	'par' => 'via',
	'participez' => 'Nehmen Sie Teil am Siteleben!',
	'plan_menu' => 'Aufbau der Webseite',
	'plus_loin' => 'Siehe auch',
	'plus_loin_title' => 'Verweise auf Websites',
	'precisezrecherche' => 'Verfeinern Sie Ihre Suche',
	'publiele' => 'Veröffentlicht die',

	// Q
	'quoideneuf' => 'Was ist neu?',

	// R
	'recherche_infructueuse' => 'Vergeblicher Suche',
	'recherche_label' => 'Suchen Sie auf der Website',
	'recherche_title' => 'Bitte geben Sie Ihre Suche',
	'recherche_title_ok' => 'Starten Sie die Suche',
	'recherche_total' => 'Gesamtzahl der Einträge gefunden',
	'recherche_value' => 'Suchen?',
	'retoursommaire' => 'Zurück Übersicht',
	'retourtop' => 'Nach oben',
	'rubrique_securisee' => 'Zugang Geoeffnet',

	// S
	'savoirplus' => 'Um mehr zu erfahren...',
	'savoirpluscritere' => 'Erfahren Sie mehr über den test',
	'sedna' => 'Sedna (RSS-feed Aggregator)',
	'sommaire' => 'Zusammenfassung',
	'syndiquer_agenda' => 'Organisieren der Tagesordnung',

	// T
	'themes' => 'Themen',
	'tousarticlesantichrono' => 'Alle Artikel in gegenuhrzeigersinn',
	'tout' => '(Ohne Filter)',
	'toutleplan' => 'Die gesamte Website auf einer Seite',

	// V
	'voirarticlespar' => 'Siehe die Artikel von',
	'voirdetailannee' => 'Die Einzelheiten des Jahres',
	'voirdetailmois' => 'Sehen Sie die Details für den Monat',
	'voirimage' => 'Das einzelne Bild sehen',
	'voirle' => 'Finden Sie unter der',
	'voirsitespar' => 'Finden Sie Websites mit',
	'vous_souhaitez_etre_tenu_au_courant' => 'Möchten Sie auf dem Laufenden gehalten werden?',
];
