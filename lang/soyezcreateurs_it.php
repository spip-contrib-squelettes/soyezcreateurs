<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/soyezcreateurs?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// A
	'aidesc' => 'Aide sur SoyezCréateurs', # MODIF

	// C
	'commencer' => 'Pour bien commencer', # MODIF
	'configurersc' => 'Configurer SoyezCréateurs', # MODIF

	// R
	'recherche_value' => 'Ricerca ?',
];
