<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function action_comptetraduction_export_dist() {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	recuperer_fond('inclure/comptetraduction_export', [
		'id_mot' => _request('id_mot'),
		'lang' => _request('lang')
	]);

	return;
}
