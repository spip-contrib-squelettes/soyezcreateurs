<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

// Pour gérer la couleur transparente avec palette
// Repris de https://git.spip.net/spip-contrib-squelettes/sarkaspip/commit/a47c098e2f6b3f64fd463d5e5886d7b5b78beb95
function ajuster_couleur_input($couleur, $type = '') {
	if (is_null($couleur)) { $couleur = '';
	}
	$transparent = ($type == 'background') ? '#ffffff' : '#000000';
	if (strtolower($couleur) == 'transparent') {
		$couleur_calculee = $transparent;
	} else { 		$couleur_calculee = $couleur;
	}

	return $couleur_calculee;
}
